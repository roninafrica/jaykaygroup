<?php

Abstract class about_us //extends DB_Model
{
    public function processController()
    {
        $count = (int)count(explode('/', $_GET['route'])); // COunt the url
        switch($count)
        {
            case 1:   // if url Dashboard
            {
                $_GET['title'] = "About - Jay Kay Group";
                $_GET['description'] = "In the west of Africa, founded in the harbor city of Accra (Ghana), Jay Kay Group is a celebrated name when it comes to anything and everything related to paper, paper products, printing and printing accessories. From a humble start at the advent of 21st century, the group has clocked a turnover of multi-million USD with offices and businesses all over Ghana, UAE and most of the countries in ECOWAS region.";
                $_GET['page'] = "About Us";
                $_GET['view'] = "about";
                return array();
                break;
            }
            default:
            {
                throw new exception('Wrong url route porcess');
            }
        }
    }
}

?>